module github.com/itchyny/mmv

go 1.16

require (
	github.com/mattn/getwild v0.0.2-0.20200919000855-c2e221927ad6
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-tty v0.0.3
	golang.org/x/sys v0.0.0-20210917161153-d61c044b1678 // indirect
)
